

@extends('layouts.master')

@section('content')
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="../public/assets/css/registration_style.css">
  <style type="text/css">
    body {
      background-image: url(http://www.joburgchiropractor.co.za/images/background.jpg);
    }
  </style>


</head>
<div class="login">

  <div class="container" style="margin-top: 5%;">
    <div class="row">
      <div class="col-md-4 container-fluid">

        <h1 class="text-center text-success"> Registracija</h1>
        <br />

        <div id="cont" class="col-sm-12">

          <ul class="nav nav-pills">



            <li class="" style="width:50%"><a class="btn btn-lg btn-default" data-toggle="tab"
                href="#home">Izdavac</a></li>

            <li class=" " style="width:48%"><a class=" btn btn-lg btn-default" data-toggle="tab"
                href="#menu1">Student</a></li>



          </ul>

          <br />


          <div class="tab-content">
            <div id="home" class="tab-pane fade in active">

              <form action="#">

                <div class="form-group">
                  <input type="text" class="form-control" id="name" placeholder="Ime">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="surname" placeholder="Prezime">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="email" placeholder="Email">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="password" placeholder="Lozinka">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="confPassword" placeholder="Potvrdite lozinku">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="mobilePhone" placeholder="Broj telefona">
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-default btn-lg">Registruj se</button>
                </div>

              </form>
              <br />
              <a href="#" class="pull-right btn btn-block btn-danger"> Imate nalog? </a>



            </div>

            <div id="menu1" class="tab-pane fade">

              <form action="#">

              <div class="regInfo container-fluid">
              <div class="basicInfo">
                  <div class="form-group">
                    <input type="text" class="form-control" id="name" placeholder="Ime">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="surname" placeholder="Prezime">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="email" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="password" placeholder="Lozinka">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="confPassword" placeholder="Potvrdite lozinku">
                  </div>
              </div>
              <div class="infos">
                <div class="form-group">
                  <input type="text" class="form-control" id="facultyName" placeholder="Ime fakulteta">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="birthDate" placeholder="Datum rodjenja">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="studyngYear" placeholder="Godina studija">

                </div>
              </div>
              </div>



                <div class="form-group">
                  <button type="submit" class="btn btn-default btn-lg">Registruj se</button>
                </div>

              </form>
              <br />
              <a href="#" class="pull-right btn btn-block btn-danger"> Imate nalog? </a>

              </form>






            </div>
          </div>
        </div>
      </div>
    </div>


  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection
