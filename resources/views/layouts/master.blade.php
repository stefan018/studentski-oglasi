<!DOCTYPE html>
<html>
    <head>
            <link rel="stylesheet" type="text/css" href="{{asset('/assets/css/index_style.css')}}">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    </head>
    <body>

        <header>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <a class="navbar-brand" href="{{route('index')}}">Studentski oglasi</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                          <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                          <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                              <a class="nav-link" href="#">Pogledaj oglase <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="#">+Postavi oglas</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{route('login')}}">Prijavi se</a>
                            </li>



                          </ul>

                        </div>
                      </nav>
        </header>

        @yield('content')

    </body>
</html>
