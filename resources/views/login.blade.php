@extends('layouts.master')

@section('content')
  <div class="container">
      <div class="row">
          <div class="col-md-5 mx-auto">
          <div id="first">
              <div class="myform form ">
                   <div class="logo mb-3">
                       <div class="col-md-12 text-center">
                          <h1>Prijava</h1>
                       </div>
                  </div>
                 <form action="" method="post" name="login">
                         <div class="form-group">
                            <input type="email" name="email"  class="form-control" id="email" aria-describedby="emailHelp" placeholder="Email">
                         </div>
                         <div class="form-group">
                            <input type="password" name="password" id="password"  class="form-control" aria-describedby="emailHelp" placeholder="Lozinka">
                         </div>

                         <div class="col-md-12 text-center ">
                            <button type="submit" class=" btn btn-block mybtn btn-primary tx-tfm">Prijava</button>
                         </div>

                  </form>

                  <a href="#" class="forgPassword"> Zaboravili ste lozinku?</a>

              </div>
          </div>

      </div>
    </div>

  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>

@endsection
