<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index()
    {
      return view('welcome');
    }

    public function login()
    {
      return view('login');
    }

    public function registration()
    {
      return view ('registration');
    }


}
