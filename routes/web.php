<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/prijava', 'PublicController@login')->name('login');
Route::get('/', 'PublicController@index')->name('index');
Route::get('/registracija', 'PublicController@registration')->name('registration');
